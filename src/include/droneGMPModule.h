#include <stdio.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <utility>
#include <string>

//ROS
#include "ros/ros.h"

//Opencv
#include "opencv2/opencv.hpp"


//ROSModule and ROSMsgs
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneModuleROS.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/points3DStamped.h"
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/droneTask.h"
#include "droneMsgsROS/droneMission.h"
#include "droneMsgsROS/droneNavData.h"
#include "droneMsgsROS/societyPose.h"
#include "droneMsgsROS/droneInfo.h"
#include "communication_definition.h"
#include "std_srvs/Empty.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/String.h"




#define FLIGHT_HEIGHT 1.3

class droneGMPROSModule : public Module
{
private:
    enum MissionType
    {
       EXPLORE,
       FIND_OBJECT,
       RESCUE
    };
    int drone_id_offset;
    int target_found_option;
    int target_found = true;
    MissionType mission_type;
    ros::NodeHandle   nh;
    std::string configFile;
    ros::Publisher droneMPcommandPub;
    std::vector<cv::Point3f> region_centroids;
    std::vector<cv::Point3f> takeoff_UAVs_points;
    std::vector<std::vector<cv::Point3f> > missions_points;
    std::vector<std::vector<std::string> > missions_tasks_names;
    int num_UAVs;
    droneMsgsROS::points3DStamped UAV_mission_points_msg;
    droneMsgsROS::vector3f UAV_task_msg;


    //droneMsgsROS::droneTask drone_task_msg;
    std::vector<droneMsgsROS::droneMission> swarm_missions;
    std::vector<int> swarm_ids;

    std::vector<ros::Publisher> swarmGMP_missions_publ;
    std::vector<ros::Publisher> swarmGMP_break_mission_publ;
    std::vector<ros::ServiceClient> swarmGMP_start_submission_planner_client;
    ros::Publisher mission_points_publ;
    ros::Publisher missions_publ;


    std::vector<ros::Subscriber> swarmGMP_subs;
    std::vector<ros::Subscriber> swarmGMP_battery_subs;
    std::vector<ros::Subscriber> swarmGMP_objective_mission_ack_subs;
    ros::Subscriber societyBroadcastSubs;

    std_srvs::Empty emptyService;
    ros::ServiceClient startSubmissionPlannerClientSrv;


public:
  droneGMPROSModule();
  ~droneGMPROSModule();
  void init();
  void random_init(float region_width, float region_height);
  void init_UAV_takeoff_points();
  void close();
  void readParameters();

  bool resetValues();
  bool startVal();
  bool stopVal();

  bool run();
  void open(ros::NodeHandle & nIn, std::string ModuleName);

  void startGlobalMissionPlannerCallback(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);
  //void subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);
  void subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg, const std::string& topic);
  void societyBroadcastCallback(const std_msgs::Int32::ConstPtr &msg);
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg, const std::string &topic);
  void subMissionPlannerTargetFoundAckCallback(const std_msgs::Int16::ConstPtr &msg, const std::string &topic);


  cv::Mat getGetPointsForSamplingArea(int num_points, float region_width, float region_height);
  std::vector<cv::Point3f> computeRegionCentroids(cv::Mat &SamplingPoints, int num_clusters);
  std::vector<std::vector<cv::Point3f> >  assignPointsToUAVs();
  std::vector<std::vector<cv::Point3f> >  sortMissionPoints(std::vector<std::vector<cv::Point3f> >& mission_points);
  void sendMissionPointsToMissionPlanner();
  std::vector<std::vector<cv::Point3f> > getMissionsPoints(){return missions_points;}
  void sendMissionToSubmissionPlanner();
  void sendBreakMissionToSubmissionPlanner();

  void generateMissions();
  std::vector<std::string> getTaskModuleNames(std::string &task_name);
  void generateMissionsTasksNames();
  std::vector<std::vector<std::string> > getMissionsTasksNames(){return missions_tasks_names;}


};
