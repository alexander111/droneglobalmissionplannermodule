#include "ros/ros.h"
#include "droneGMPModule.h"
#include "nodes_definition.h"
using namespace std;

int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, "droneGlobalMissionPlanner");
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << "droneGlobalMissionPlannerROSModule" << std::endl;

    droneGMPROSModule MyGMPROSModule;
    MyGMPROSModule.open(n, "droneGlobalMissionPlanner");


    //ros::Publisher drone_gmp_ack_pub = n.advertise<std_msgs::Bool>("globalMissionPlannerAck",1,true);


    float region_width = 20;
    float region_height = 30;
    cv::Mat RandomPoints = MyGMPROSModule.getGetPointsForSamplingArea(100000, region_width, region_height);
    //cout<<"Random Points Generated:"<<endl;
    //cout<<RandomPoints<<endl;

    std::vector<cv::Point3f> Centroids = MyGMPROSModule.computeRegionCentroids(RandomPoints, 24);
    cout<<endl<<"Centroids:"<<endl;
    for(int i=0;i<Centroids.size();i++)
        cout<<"x: "<<Centroids[i].x<<" ; "<<"y: "<<Centroids[i].y<<" ; "<<"z: "<<Centroids[i].z<<endl;
    cout<<endl<<endl;

    //MyGMPROSModule.random_init(region_width, region_height);
    MyGMPROSModule.init_UAV_takeoff_points();
    MyGMPROSModule.assignPointsToUAVs();
    MyGMPROSModule.generateMissionsTasksNames();
    MyGMPROSModule.generateMissions();

    MyGMPROSModule.sendMissionToSubmissionPlanner();

    //MyGMPROSModule.sendMissionPointsToMissionPlanner();


    try
    {
        while(ros::ok())
        {
            ros::spinOnce();
            if(MyGMPROSModule.run())
            {

            }
            MyGMPROSModule.sleep();
        }
        return 1;
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

}
