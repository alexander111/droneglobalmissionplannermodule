#include "droneGMPModule.h"
using namespace std;

droneGMPROSModule::droneGMPROSModule():
  Module(droneModule::active, 15.0)
{
    drone_id_offset = 10;
    num_UAVs = 6;
    target_found = 0;
    target_found_option = 4;
    std::cout << "GlobalMissionPlannerROSModule(..), enter and exit" << std::endl;
    mission_type = MissionType::FIND_OBJECT;
    return;
}

droneGMPROSModule::~droneGMPROSModule()
{
  close();
  return;
}

void droneGMPROSModule::init()
{

}

void droneGMPROSModule::init_UAV_takeoff_points()
{
    for(int i=0;i<num_UAVs;i++)
    {
        cv::Point3f r;
        std::vector<std::string> task_name;
        switch(i)
        {

//            case 0:
//                r.x =  2;
//                r.y =  1;
//                r.z = FLIGHT_HEIGHT;
//                break;
//            case 1:
//                r.x =  7.0;
//                r.y =  1.0;
//                r.z = FLIGHT_HEIGHT;
//                break;
//            case 2:
//                r.x =  4.5;
//                r.y =  8.0;
//                r.z = FLIGHT_HEIGHT;
//                break;


        //Mission of 20x20m whith 4 UAV
//            case 0:
//                r.x =  2.0;
//                r.y =  1.0;
//                r.z = FLIGHT_HEIGHT;
//                break;
//            case 1:
//                r.x =  7.0;
//                r.y =  1.0;
//                r.z = FLIGHT_HEIGHT;
//                break;
//            case 2:
//                r.x =  13.0;
//                r.y =  1.0;
//                r.z = FLIGHT_HEIGHT;
//                break;
//            case 3:
//                r.x =  18.0;
//                r.y =  1.0;
//                r.z = FLIGHT_HEIGHT;
//                break;

        case 0:
            r.x =  2.0;
            r.y =  1.0;
            r.z = FLIGHT_HEIGHT;
            break;
        case 1:
            r.x =  10.0;
            r.y =  1.0;
            r.z = FLIGHT_HEIGHT;
            break;
        case 2:
            r.x =  12.0;
            r.y =  1.0;
            r.z = FLIGHT_HEIGHT;
            break;
        case 3:
            r.x =  2.0;
            r.y =  29.0;
            r.z = FLIGHT_HEIGHT;
            break;
        case 4:
            r.x =  10.0;
            r.y =  29.0;
            r.z = FLIGHT_HEIGHT;
            break;
        case 5:
            r.x =  17.0;
            r.y =  29.0;
            r.z = FLIGHT_HEIGHT;
            break;
        }

        takeoff_UAVs_points.push_back(r);

        task_name.push_back("Takeoff");
        task_name.push_back("Hover");
        task_name.push_back("Land");
        missions_tasks_names.push_back(task_name);

    }

    cout<<"UAV Takeoff Positions:"<<endl;
    for(int i=0;i<takeoff_UAVs_points.size();i++)
        cout<<"x: "<<takeoff_UAVs_points[i].x<< " ; "<<"y: "<<takeoff_UAVs_points[i].y<< " ; "<<"z: "<<takeoff_UAVs_points[i].z<<endl;
    cout<<endl<<endl;
}

void droneGMPROSModule::random_init(float region_width, float region_height)
{
    static bool first = true;
    if ( first )
    {
       srand(time(NULL));
       first = false;
    }

    for(int i=0;i<num_UAVs;i++)
    {
        std::vector<std::string> task_name;
        cv::Point3f r;
        r.x =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_width));
        r.y =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_height));
        r.z = FLIGHT_HEIGHT;

        takeoff_UAVs_points.push_back(r);

        task_name.push_back("Takeoff");
        task_name.push_back("Hover");
        task_name.push_back("Land");
        missions_tasks_names.push_back(task_name);
    }

    cout<<"UAV Takeoff Positions:"<<endl;
    for(int i=0;i<takeoff_UAVs_points.size();i++)
        cout<<"x: "<<takeoff_UAVs_points[i].x<< " ; "<<"y: "<<takeoff_UAVs_points[i].y<< " ; "<<"z: "<<takeoff_UAVs_points[i].z<<endl;
    cout<<endl<<endl;

}


void droneGMPROSModule::close()
{

    return;
}

bool droneGMPROSModule::resetValues()
{
    if(!Module::resetValues())
        return false;

    return true;
}

bool droneGMPROSModule::startVal()
{
  if(!Module::startVal())
    return false;

  for(int i=0;i<swarm_missions.size();i++)
  {
      //For starting the subMissionPlanner
      if(i > 0)
          ros::Duration(0).sleep();
      if(!swarmGMP_start_submission_planner_client[i].call(emptyService))
        return false;
  }

   return true;
}

bool droneGMPROSModule::stopVal()
{
    if(!Module::stopVal())
        return false;

     return true;
}

bool droneGMPROSModule::run()
{
  //TODO: Run the global mission planner algo here
  if(!Module::run())
    return false;


  return true;
}

void droneGMPROSModule::readParameters()
{
  //TODO: Read the parameters from the launch files
  return;
}


void droneGMPROSModule::open(ros::NodeHandle &nIn, std::string ModuleName)
{
    nh = nIn;

    Module::open(nh,ModuleName);


    readParameters();
    init();

    //TODO: publish and subscribe all the ROS topics here
    //mission_points_publ = nh.advertise<droneMsgsROS::droneMission>("globalMissionPlannerMission", 1, true);
    societyBroadcastSubs = n.subscribe("societyBroadcast", 100, &droneGMPROSModule::societyBroadcastCallback, this);


    missions_publ = nh.advertise<droneMsgsROS::droneMission>("globalMissionPlannerMission", 1, true);
    //startSubmissionPlannerClientSrv = nh.serviceClient<std_srvs::Empty>("startDroneSubmissionPlanner");


    for(int i=drone_id_offset;i<(num_UAVs+drone_id_offset);i++)
    {
        std::string topic_publ_name = std::string("/drone")+std::to_string(i)+"/"+"globalMissionPlannerMission";
        std::string topic_subs_name = std::string("/drone")+std::to_string(i)+"/"+"subMissionPlannerState";
        std::string topic_battery_subs_name = std::string("/drone")+std::to_string(i)+"/"+"battery";
        std::string topic_target_found_ack_subs_name = std::string("/drone")+std::to_string(i)+"/"+"globalMissionPlannerTargetFoundAck";
        std::string topic_break_mission_name = std::string("/drone")+std::to_string(i)+"/"+"globalMissionPlannerBreakMissionAck";
        std::string start_submission_planner_client = std::string("/drone")+std::to_string(i)+"/"+"droneSubMissionPlanner/start";

        ros::Publisher swarm_missions_publ = nh.advertise<droneMsgsROS::droneMission>(topic_publ_name, 1, true);
        ros::Publisher swarm_target_found_publ = nh.advertise<std_msgs::Bool>(topic_break_mission_name, 1, true);
        startSubmissionPlannerClientSrv = nh.serviceClient<std_srvs::Empty>(start_submission_planner_client,1);

        swarmGMP_missions_publ.push_back(swarm_missions_publ);
        swarmGMP_break_mission_publ.push_back(swarm_target_found_publ);
        swarmGMP_start_submission_planner_client.push_back(startSubmissionPlannerClientSrv);

        //ros::Subscriber subs = nh.subscribe(topic_subs_name, 100, &droneGMPROSModule::subMissionPlannerStateCallback, this);
        ros::Subscriber subs = nh.subscribe<droneMsgsROS::droneStatus>(topic_subs_name, 1000, boost::bind(&droneGMPROSModule::subMissionPlannerStateCallback, this, _1, topic_subs_name));
        ros::Subscriber batterySubs = nh.subscribe<droneMsgsROS::battery>(topic_battery_subs_name, 1, boost::bind(&droneGMPROSModule::batteryCallback, this, _1, topic_battery_subs_name));
        ros::Subscriber subMissionPlanner_target_found_AckSubs = nh.subscribe<std_msgs::Int16>(topic_target_found_ack_subs_name, 1, boost::bind(&droneGMPROSModule::subMissionPlannerTargetFoundAckCallback, this, _1, topic_target_found_ack_subs_name));
        swarmGMP_subs.push_back(subs);
        swarmGMP_battery_subs.push_back(batterySubs);
        swarmGMP_objective_mission_ack_subs.push_back(subMissionPlanner_target_found_AckSubs);

    }

    droneModuleOpened = true;

    return;
}

void droneGMPROSModule::societyBroadcastCallback(const std_msgs::Int32::ConstPtr &msg)
{

}


//void droneGMPROSModule::subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg)
void droneGMPROSModule::subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg, const std::string& topic)
{
//  const std::string& publisher_name = event.getPublisherName();
//  ROS_INFO("publisher: %s\ttopic: %s", publisher_name.c_str(), topic.c_str());
}

void droneGMPROSModule::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg, const std::string& topic)
{
    //batteryMsgs=*msg;
    //ROS_INFO("battery topic: %s", topic.c_str());
    return;
}

void droneGMPROSModule::subMissionPlannerTargetFoundAckCallback(const std_msgs::Int16::ConstPtr &msg, const std::string &topic)
{
    ROS_INFO("Traget Found topic: %s", topic.c_str());
    int id_drone = msg->data - drone_id_offset;
    if(1)
    {
        target_found++;
        if(target_found > 1)
            return;
        missions_tasks_names.clear();
        missions_points.clear();
        if(mission_type == MissionType::FIND_OBJECT)
        {
            cout<<"Object Found!"<<endl;
            cout<<"Assigning NEW MISSION..."<<endl;
            for(int i=0;i<num_UAVs;i++)
            {
                std::vector<std::string> task_name;
                std::vector<cv::Point3f> takeoff_point;
                std::vector<cv::Point3f> altitude_point;
                switch(target_found_option)
                {
                    case 1:

                        task_name.push_back("GoHome");
                        task_name.push_back("Land");
                        missions_tasks_names.push_back(task_name);


                        takeoff_point.push_back(takeoff_UAVs_points[i]);
                        missions_points.push_back(takeoff_point);
                        break;

                    case 2:
                        if(i == id_drone)
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("Flip");
                            task_name.push_back("Hover");
                            task_name.push_back("Land");
                        }
                        else
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("Land");
                        }
                        missions_tasks_names.push_back(task_name);
                        break;

                    case 3:
                        if(i == id_drone)
                        {
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("Land");

                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                        }
                        else
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("Land");

                            altitude_point.push_back(cv::Point3f(0,0,0.0));
                            altitude_point.push_back(cv::Point3f(0,0,0.0));
                        }
                        missions_points.push_back(altitude_point);
                        missions_tasks_names.push_back(task_name);
                        break;

                    case 4:
                        if(i == id_drone)
                        {
                            task_name.push_back("Hover");
//                            task_name.push_back("Flip");
//                            task_name.push_back("Hover");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("GoHome");
                            task_name.push_back("Land");


                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(takeoff_UAVs_points[i]);
                        }
                        else
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("GoHome");
                            task_name.push_back("Land");

                            altitude_point.push_back(takeoff_UAVs_points[i]);
                        }
                        missions_points.push_back(altitude_point);
                        missions_tasks_names.push_back(task_name);
                        break;

                }
            }

        }

//        cout<<"*** Missions Names ***"<<endl;
//        for(int i=0;i<missions_tasks_names.size();i++)
//        {
//            for(int j=0;j<missions_tasks_names[i].size();j++)
//                cout<<missions_tasks_names[i][j]<<endl;
//            cout<<endl<<endl;
//        }

//        cout<<"*** UAV_mission_points FINAL ***"<<endl;
//        for(int i=0;i<missions_points.size();i++)
//        {
//            for(int j=0;j<missions_points[i].size();j++)
//                cout<<missions_points[i][j]<<endl;
//            cout<<endl<<endl;
//        }

        generateMissions();

        sendBreakMissionToSubmissionPlanner();
        sendMissionToSubmissionPlanner();

    }
}

cv::Mat droneGMPROSModule::getGetPointsForSamplingArea(int num_points, float region_width, float region_height)
{
    cv::Mat random_points(num_points,2,CV_32F);

    static bool first = true;
    if ( first )
    {
       srand(time(NULL));
       first = false;
    }

    for(int i=0;i<num_points;i++)
    {
        random_points.at<float>(i,0) =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_width));
        random_points.at<float>(i,1) =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_height));
    }

    return random_points;

}

std::vector<cv::Point3f> droneGMPROSModule::computeRegionCentroids(cv::Mat &SamplingPoints, int num_clusters)
{
    //cv::Mat I = cv::Mat::zeros(300,200,CV_8U);
    cv::Mat Centroids;
    cv::Mat Labels;


    double compactness=0.0;
    cv::TermCriteria criteria_kmeans(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER,50,0.02);
    compactness = cv::kmeans(SamplingPoints, num_clusters, Labels, criteria_kmeans, 5, cv::KMEANS_RANDOM_CENTERS, Centroids);

    cout<<"Centroids Mat:"<<endl<<Centroids<<endl;
    for(int i=0;i<Centroids.rows;i++)
        region_centroids.push_back(cv::Point3f(Centroids.at<float>(i,0),Centroids.at<float>(i,1), FLIGHT_HEIGHT));

    return region_centroids;
}

std::vector<std::vector<cv::Point3f> > droneGMPROSModule::assignPointsToUAVs()
{
    std::vector<std::vector<cv::Point3f> > UAV_mission_points;
    std::vector<std::vector<cv::Point3f> > UAV_mission_points_v2;
    std::vector<float> distances_to_mission_points;
    std::vector<int> centroids_id;

    for(int i=0;i<region_centroids.size();i++)
    {
        std::vector<float> distances;
        for(int j=0;j<takeoff_UAVs_points.size();j++)
            distances.push_back(cv::norm(region_centroids[i] - takeoff_UAVs_points[j]));

//        for(int z=0;z<distances.size();z++)
//            cout<<distances[z]<<endl;


        distances_to_mission_points.push_back(*std::min_element(distances.begin(), distances.end()));
        centroids_id.push_back(std::min_element(distances.begin(), distances.end()) - distances.begin() + 1);
//        cout<<"Minimum element idx: "<<centroids_id[i]<<endl;
//        cout<<"Distance to Minimum element: "<<distances_to_mission_points[i]<<endl;
//        cout<<endl<<endl;


    }
    cout<<"Points Assigned!"<<endl;

    cout<<"ID de los Centroides:"<<endl;
    for(int i=0;i<centroids_id.size();i++)
        cout<<centroids_id[i]<<endl;

//    cout<<"distances_to_mission_points: "<<endl;
//    for(int i=0;i<distances_to_mission_points.size();i++)
//        cout<<distances_to_mission_points[i]<<endl;

    for(int i=0;i<num_UAVs;i++)
    {
        std::vector<cv::Point3f> p;
        std::vector<float> dist;
        for(int j=0;j<region_centroids.size();j++)
        {
            if(centroids_id[j] == (i+1))
            {
                p.push_back(region_centroids[j]);
                dist.push_back(distances_to_mission_points[j]);
            }
        }
        UAV_mission_points_v2.push_back(p);

        //Sort the vector p according to the distance
        std::vector<int> ind(dist.size());
        std::iota(ind.begin(), ind.end(), 0);
        auto comparator = [&dist](float a, float b){ return dist[a] < dist[b]; };
        std::sort(ind.begin(), ind.end(), comparator);

        std::vector<cv::Point3f> vCopy = p;
        for(int z = 0; z < ind.size(); ++z)
            p[z] = vCopy[ind[z]];

        cout<<"indices vector ordenado: "<<endl;
        for(int z=0;z<dist.size();z++)
            cout<<ind[z]<<endl;


        UAV_mission_points.push_back(p);
    }

    cout<<"*** UAV_mission_points ***"<<endl;
    for(int i=0;i<UAV_mission_points_v2.size();i++)
    {
        for(int j=0;j<UAV_mission_points_v2[i].size();j++)
            cout<<UAV_mission_points_v2[i][j]<<endl;
        cout<<endl<<endl;
    }

    cout<<"*** UAV_mission_points SORTED***"<<endl;
    for(int i=0;i<UAV_mission_points.size();i++)
    {
        for(int j=0;j<UAV_mission_points[i].size();j++)
            cout<<UAV_mission_points[i][j]<<endl;
        cout<<endl<<endl;
    }


    missions_points = sortMissionPoints(UAV_mission_points);

    cout<<"*** UAV_mission_points FINAL SORTED***"<<endl;
    for(int i=0;i<missions_points.size();i++)
    {
        for(int j=0;j<missions_points[i].size();j++)
            cout<<missions_points[i][j]<<endl;
        cout<<endl<<endl;
    }


    return missions_points;
}

std::vector<std::vector<cv::Point3f> >  droneGMPROSModule::sortMissionPoints(std::vector<std::vector<cv::Point3f> >& mission_points)
{
    std::vector<std::vector<cv::Point3f> > mission_points_sorted = mission_points;
    for(int i=0;i< mission_points_sorted.size();i++)
    {
        for(int j=0;j<mission_points_sorted[i].size()-1;j++)
        {
            std::vector<float> distances;
            for(int z=j;z<mission_points_sorted[i].size()-1;z++)
                distances.push_back(cv::norm(mission_points_sorted[i][j] - mission_points_sorted[i][z+1]));

            int new_idx = std::min_element(distances.begin(), distances.end()) - distances.begin() + 1 + j;
            std::iter_swap(mission_points_sorted[i].begin() + j + 1, mission_points_sorted[i].begin() + new_idx);

        }
    }

    return mission_points_sorted;
}

void droneGMPROSModule::sendMissionPointsToMissionPlanner()
{
    UAV_mission_points_msg.header.stamp = ros::Time::now();
    for(int i=0;i<missions_points.size();i++)
    {
        for(int j=0;j<missions_points[i].size();j++)
        {
            UAV_task_msg.x = missions_points[i][j].x;
            UAV_task_msg.y = missions_points[i][j].y;
            UAV_task_msg.z = missions_points[i][j].z;
            UAV_mission_points_msg.points3D.push_back(UAV_task_msg);
        }

        mission_points_publ.publish(UAV_mission_points_msg);
        if(i==0)
            break;
    }

}

std::vector<std::string>  droneGMPROSModule::getTaskModuleNames(std::string &task_name)
{
    std::vector<std::string> modules_names;

    if(task_name == "Takeoff")
    {
        modules_names.push_back("droneSpeechROSModule");
        modules_names.push_back("droneSoundROSModule");
    }
    else if(task_name == "Hover")
    {
        modules_names.push_back(MODULE_NAME_TRAJECTORY_PLANNER);
        modules_names.push_back(MODULE_NAME_ARUCO_EYE);
        modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
        modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
        modules_names.push_back(MODULE_NAME_LOCALIZER);
        modules_names.push_back("droneSpeechROSModule");
        modules_names.push_back("droneObstacleDistanceCalculator");
        modules_names.push_back("droneSoundROSModule");
    }
    else if(task_name == "MoveToPoint" || task_name == "TurnInYaw" || task_name == "Sleep" || task_name == "GoHome" || task_name == "Flip")
    {
        modules_names.push_back(MODULE_NAME_TRAJECTORY_PLANNER);
        modules_names.push_back(MODULE_NAME_ARUCO_EYE);
        modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
        modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
        modules_names.push_back(MODULE_NAME_YAW_PLANNER);
        modules_names.push_back(MODULE_NAME_LOCALIZER);
        modules_names.push_back("droneSpeechROSModule");
        modules_names.push_back("droneObstacleDistanceCalculator");
        modules_names.push_back("droneSoundROSModule");
    }

    else if(task_name == "Land")
    {
        modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
        modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
        modules_names.push_back("droneSpeechROSModule");
        modules_names.push_back("droneSoundROSModule");
    }



    return modules_names;
}

void droneGMPROSModule::generateMissionsTasksNames()
{
    switch(mission_type)
    {
        case MissionType::EXPLORE:
            for(int i=0;i<missions_points.size();i++)
            {
                for(int j=0;j<missions_points[i].size();j++)
                {
                    missions_tasks_names[i].push_back("MoveToPoint");
                }
            }
            break;
        case MissionType::FIND_OBJECT:
            for(int i=0;i<missions_points.size();i++)
            {
                for(int j=0;j<missions_points[i].size();j++)
                {
                    missions_tasks_names[i].push_back("MoveToPoint");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                }
            }
            break;

    }

    for(int i=0;i<missions_tasks_names.size();i++)
    {
        for(int j=1;j<missions_tasks_names[i].size();j++)
        {
            if(missions_tasks_names[i][j-1] == "Land")
                std::iter_swap(missions_tasks_names[i].begin() + (j -1), missions_tasks_names[i].begin() + j);
        }
    }


    cout<<"*** Missions Names ***"<<endl;
    for(int i=0;i<missions_tasks_names.size();i++)
    {
        for(int j=0;j<missions_tasks_names[i].size();j++)
            cout<<missions_tasks_names[i][j]<<endl;
        cout<<endl<<endl;
    }
}

void droneGMPROSModule::generateMissions()
{
    swarm_missions.clear();
    for(int i=0;i<missions_tasks_names.size();i++)
    {
        droneMsgsROS::droneMission drone_mission_msg;
        drone_mission_msg.header.stamp = ros::Time::now();
        int move_to_point_counter = 0;
        int turn_in_yaw_counter = 0;
        for(int j=0;j<missions_tasks_names[i].size();j++)
        {
            if(turn_in_yaw_counter > 3)
                turn_in_yaw_counter = 0;
            droneMsgsROS::droneTask drone_task_msg;
            if(missions_tasks_names[i][j] == "Takeoff")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::TAKE_OFF;
                drone_task_msg.speech_name = "Despegando";
                //drone_task_msg.yaw = -500;
            }

            else if(missions_tasks_names[i][j] == "Hover")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::HOVER;
                drone_task_msg.time = 2;
                //drone_task_msg.yaw = -500;
            }

            else if(missions_tasks_names[i][j] == "MoveToPoint")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_TRAJECTORY;
                drone_task_msg.speech_name = "Movimiento en trayectoria";
                drone_task_msg.yawSelector = 2;
                drone_task_msg.point.x = missions_points[i][move_to_point_counter].x;
                drone_task_msg.point.y = missions_points[i][move_to_point_counter].y;
                drone_task_msg.point.z = missions_points[i][move_to_point_counter].z;
                drone_task_msg.pointToLook.x = missions_points[i][move_to_point_counter].x;
                drone_task_msg.pointToLook.y = missions_points[i][move_to_point_counter].y;
                //drone_task_msg.yaw = -500;
                move_to_point_counter++;
            }

            else if(missions_tasks_names[i][j] == "TurnInYaw")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_TRAJECTORY;
                drone_task_msg.speech_name = "Girando en yaw";
                drone_task_msg.yawSelector = 1;
                switch(turn_in_yaw_counter)
                {
                    case 0:
                        drone_task_msg.yaw = 180;
                        break;
                    case 1:
                        drone_task_msg.yaw = 270;
                        break;
                    case 2:
                        drone_task_msg.yaw = 0;
                        break;
                    case 3:
                        drone_task_msg.yaw = 90;
                        break;
                }
                turn_in_yaw_counter++;
            }

            else if(missions_tasks_names[i][j] == "Sleep")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::SLEEP;
                if(i>0)
                    drone_task_msg.time = 2;
                else
                    drone_task_msg.time = 2;
                //drone_task_msg.yaw = -500;
            }

            else if(missions_tasks_names[i][j] == "Land")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::LAND;
                drone_task_msg.speech_name = "Aterrizar";
                //drone_task_msg.yaw = -500;

            }

            else if(missions_tasks_names[i][j] == "GoHome")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_TRAJECTORY;
                drone_task_msg.speech_name = "Retorno al punto inicial";
                drone_task_msg.point.x = missions_points[i][move_to_point_counter].x;
                drone_task_msg.point.y = missions_points[i][move_to_point_counter].y;
                drone_task_msg.point.z = missions_points[i][move_to_point_counter].z;
                drone_task_msg.yawSelector = 2;
                drone_task_msg.pointToLook.x = missions_points[i][move_to_point_counter].x;
                drone_task_msg.pointToLook.y = missions_points[i][move_to_point_counter].y;
                move_to_point_counter++;
            }

            else if(missions_tasks_names[i][j] == "Flip")
            {
                drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_FLIP_RIGHT;
                drone_task_msg.speech_name = "Flip";
            }
            drone_task_msg.module_names = getTaskModuleNames(missions_tasks_names[i][j]);
            drone_mission_msg.mission.push_back(drone_task_msg);

        }

        swarm_missions.push_back(drone_mission_msg);

    }
}

void droneGMPROSModule::sendMissionToSubmissionPlanner()
{
    for(int i=0;i<swarm_missions.size();i++)
    {
//        missions_publ.publish(swarm_missions[i]);
//        if(i==0)
//            break;

        //For publishing the Mission to each UAV of the Swarm
        cout<<"*** swarm_missions ["<<i<<"] ***"<<swarm_missions[i]<<endl<<endl;
        swarmGMP_missions_publ[i].publish(swarm_missions[i]);
    }
}

void droneGMPROSModule::sendBreakMissionToSubmissionPlanner()
{
    std_msgs::Bool break_mission_msg;
    break_mission_msg.data = true;
    for(int i=0;i<swarm_missions.size();i++)
    {
        //For publishing the Mission to each UAV of the Swarm
        swarmGMP_break_mission_publ[i].publish(break_mission_msg);
    }
}








